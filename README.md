# DH6010 Data Visualisation App 

This application supports the [Getting Started with Node on Heroku](https://devcenter.heroku.com/articles/getting-started-with-nodejs) article - check it out.

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) and the [Heroku Toolbelt](https://toolbelt.heroku.com/) installed.

```sh
$ git clone https://MuddyGames@bitbucket.org/MuddyGames/dh6010.git
$ cd dh6010
$ npm install
$ npm install chart.js --save
$ npm install xlsx --save
$ npm install jquery --save
$ npm start
```

Your app should now be running on [localhost:5000](http://localhost:5000/).

Open a [Chart](http://localhost:5000/?page_title=Planning%20Permissions%20Granted%20from%202006%20to%202015&chart_title=Granted&filename=Planning-Permissions-Granted.xlsx&chart_type=line&border_Color=rgba(220,192,192,1)).

## Deploying to Heroku

```
$ heroku create
$ git push heroku master
$ heroku open
```
or

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

## Documentation

For more information about this Data Visualisation tool see the usage guidelines:

- [Sample Usage](https://dh6010.herokuapp.com/usage)
- [Data Visualisation Open-Source Tool:Webcharts using Excel](https://makingmuddygames.blogspot.ie/2017/03/data-visualisation-open-source.html)

For more information about using Node.js on Heroku, see these Dev Center articles:

- [Getting Started with Node.js on Heroku](https://devcenter.heroku.com/articles/getting-started-with-nodejs)
- [Heroku Node.js Support](https://devcenter.heroku.com/articles/nodejs-support)
- [Node.js on Heroku](https://devcenter.heroku.com/categories/nodejs)
- [Best Practices for Node.js Development](https://devcenter.heroku.com/articles/node-best-practices)
- [Using WebSockets on Heroku with Node.js](https://devcenter.heroku.com/articles/node-websockets)