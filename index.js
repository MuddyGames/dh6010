var express = require('express');
var app = express();

app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/data'));

app.use('/engine/jquery/scripts', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/engine/chart.js/scripts', express.static(__dirname + '/node_modules/chart.js/dist'));
app.use('/engine/xlsx/scripts', express.static(__dirname + '/node_modules/xlsx/dist'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
  response.render('pages/index');
});

app.get('/usage', function(request, response) {
  response.render('pages/usage');
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});


